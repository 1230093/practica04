package com.androidbegin.practica04;

import com.androidbegin.fragmenttabstutorial.R;

import android.app.ActionBar;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;


public class MainActivity extends Activity {
	
	public final static String EXTRA_MESSAGE = "com.androidbegin.fragmenttabstutorial";
	// Declaricion de tabs
	
	ActionBar.Tab Tab1, Tab2, Tab3;
	
	Fragment fragmentTab1 = new FragmentTab1();
	Fragment fragmentTab2 = new FragmentTab2();
	Fragment fragmentTab3 = new FragmentTab3();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		ActionBar actionBar = getActionBar();

		// Ocultando el icono del action bar
		actionBar.setDisplayShowHomeEnabled(false);

		// Ocultando el titulo del action bar
		actionBar.setDisplayShowTitleEnabled(false);

		// Creando un aciont bar
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		// asignando un nombre las tabs
		Tab1 = actionBar.newTab().setText("Tab1");
		Tab2 = actionBar.newTab().setText("Tab2");
		Tab3 = actionBar.newTab().setText("Tab3");

		// asignando tabs 
		Tab1.setTabListener(new TabListener(fragmentTab1));
		Tab2.setTabListener(new TabListener(fragmentTab2));
		Tab3.setTabListener(new TabListener(fragmentTab3));

		// Agregar tabs a la barra
		actionBar.addTab(Tab1);
		actionBar.addTab(Tab2);
		actionBar.addTab(Tab3);
	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_activity_actions, menu);
        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	 // Handle presses on the action bar items
    	 switch (item.getItemId()) {
    	 case R.id.action_search:
    	 openSearch();
    	 return true;
    	 case R.id.menu_settings:
    	 openSettings();
    	 return true;
    	 default:
    	 return super.onOptionsItemSelected(item);
    	 }

    }
    
    private void openSearch() {
        Toast.makeText(this, "Search button pressed", Toast.LENGTH_SHORT).show();
    }
    
    private void openSettings() {
        Toast.makeText(this, "Settings button pressed", Toast.LENGTH_SHORT).show();
    }
    
    public class FragmentTab1 extends Fragment implements View.OnClickListener {
        
        View rootView;
        
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            rootView = inflater.inflate(R.layout.fragmenttab1, container, false);
            
            Button btn = (Button) rootView.findViewById(R.id.btnconvercion);
            btn.setOnClickListener(this);
            
            return rootView;
            
         
        }
    	@Override
    	public void onClick(View v) {
    		// TODO Auto-generated method stub
    		
    		switch(v.getId()){
    		
    		case R.id.btnconvercion:
    			EditText editText = (EditText) findViewById(R.id.convercion1);
    	    	String message = editText.getText().toString();
    	    	Integer numero = Integer.parseInt(message);
    	    	Integer Salida = (int) ((numero - 32.00)/1.800);
    	    	String msje2 = "Temperatura en Grados Centigrados: " + Salida ;
    	    	((TextView)rootView.findViewById(R.id.myTextView)).setText((msje2));
    			break;
    		}
    	}
     
    }
	 
    public class FragmentTab2 extends Fragment implements View.OnClickListener {
        
        View rootView;
        
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            rootView = inflater.inflate(R.layout.fragmenttab2, container, false);
            
            Button btn = (Button) rootView.findViewById(R.id.btnCalcular);
            btn.setOnClickListener(this);
            
            return rootView;
            
         
        }
    	@Override
    	public void onClick(View v) {
    		// TODO Auto-generated method stub
    		
    		switch(v.getId()){
    		
    		case R.id.btnCalcular:
    			
    			EditText editText = (EditText) findViewById(R.id.lado1);
    			EditText editText2 = (EditText) findViewById(R.id.lado2);
    			
    	    	String message = editText.getText().toString();
    	    	String message2 = editText2.getText().toString();
    	    	
    	    	Integer numero = Integer.parseInt(message);
    	    	Integer numero2 = Integer.parseInt(message2);
    	    	
    	    	Integer Salida = (int) (numero * numero2);
    	    	String msje2 = "El area del cuadrado es: " + Salida ;
    	    	((TextView)rootView.findViewById(R.id.myTextView)).setText((msje2));
    			break;
    		}
    	}
     
    }
	
    public class FragmentTab3 extends Fragment implements View.OnClickListener {
        
        View rootView;
        
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            rootView = inflater.inflate(R.layout.fragmenttab3, container, false);
            
            Button btn = (Button) rootView.findViewById(R.id.btnConvertir);
            btn.setOnClickListener(this);
            
            return rootView;
            
         
        }
    	@Override
    	public void onClick(View v) {
    		// TODO Auto-generated method stub
    		
    		switch(v.getId()){
    		
    		case R.id.btnConvertir:
    			
    			EditText editText = (EditText) findViewById(R.id.millas);
    			
    	    	String message = editText.getText().toString();
    	    	
    	    	Integer numero = Integer.parseInt(message);
    	    	
    	    	
    	    	Integer Salida = (int) (numero * 1.60934 );
    	    	String msje2 = numero + "milas son : " + Salida + "kilometros.";
    	    	((TextView)rootView.findViewById(R.id.myTextView)).setText((msje2));
    			break;
    		}
    	}
     
    }
	 
}
